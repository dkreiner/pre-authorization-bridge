package api

import (
	"encoding/json"
	"io"
	"net/http"
	"preauthbridge/internal/config"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

type MockWellKnown struct {
	Issuer              string   `mapstructure:"issuer"`
	TokenEndpoint       string   `mapstructure:"token_endpoint"`
	GrantTypesSupported []string `mapstructure:"grant_types_supported"`
}

func setupServer() *sync.WaitGroup {
	var wg sync.WaitGroup
	wg.Add(1)
	go startRest(8080, &wg)

	// Wait until server is presumably up
	waitForServer("http://localhost:8080/health_check")

	return &wg
}

func waitForServer(url string) {
	for {
		resp, err := http.Head(url)
		if err == nil && resp.StatusCode == http.StatusOK {
			break
		}
		// if status is not 200 or there's an error, wait a bit and try again
		time.Sleep(time.Millisecond * 100)
	}
}

func cleanupServer(wg *sync.WaitGroup) {
	shutdown()
	wg.Wait()
}

func TestWellKnownEndpoint(t *testing.T) {
	var responseObj MockWellKnown
	// mock config values of well known
	config.CurrentPreAuthBridgeConfig.WellKnown = MockWellKnown{Issuer: "http://localhost:8080", TokenEndpoint: "http://localhost:8080/token", GrantTypesSupported: []string{"urn:ietf:params:oauth:grant-type:pre-authorized_code"}}
	// start the server
	var wg = setupServer()

	resp, err := http.Get("http://localhost:8080/.well-known/openid-configuration")
	require.NoError(t, err)

	defer func() {
		err := resp.Body.Close()
		require.NoError(t, err)
	}()

	// Check the status code
	require.Equal(t, http.StatusOK, resp.StatusCode)

	// Read the response body
	bodyBytes, err := io.ReadAll(resp.Body)
	require.NoError(t, err)

	// Get the response body values
	err = json.Unmarshal(bodyBytes, &responseObj)
	require.NoError(t, err)

	// Assert the values
	require.Equal(t, "http://localhost:8080", responseObj.Issuer)
	require.Equal(t, "http://localhost:8080/token", responseObj.TokenEndpoint)
	require.Equal(t, []string{"urn:ietf:params:oauth:grant-type:pre-authorized_code"}, responseObj.GrantTypesSupported)

	// This will run even if the test fails or panics
	t.Cleanup(func() { cleanupServer(wg) })
}
