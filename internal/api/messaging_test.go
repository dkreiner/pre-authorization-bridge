package api

import (
	"fmt"
	"testing"
)

func TestTwoFactorCheck(t *testing.T) {
	testCases := []struct {
		recipientType    RecipientType
		recipientAddress string
		valid            bool
	}{
		{"email", "test@gmail.com", true},
		{"email", "test!test@@foo.to", false},
		{"didComm", "did:example:1234sdfa567sgfsa890", true},
		{"didComm", "did:example::1234sdfa567sgfsa890", false},
		{"didComm", "test@gmail.com", false},
		{"", "", false},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("type: %s, valid: %t", tc.recipientType, tc.valid), func(t *testing.T) {
			isValid, err := checkTwoFactor(tc.recipientType, tc.recipientAddress)
			if isValid != tc.valid {
				t.Fatalf("two factor should be valid but is not: %v", err)
			} else {
				if err != nil && tc.valid == true {
					t.Errorf("unexpected error occured: %v", err)
				}
			}
		})
	}
}
