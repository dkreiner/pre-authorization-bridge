package api

import (
	"context"
	"github.com/gofiber/fiber/v2"
	log "github.com/sirupsen/logrus"
	"preauthbridge/internal/config"
	"preauthbridge/internal/entity"
	"preauthbridge/internal/token"
	"strconv"
	"sync"
)

var app *fiber.App

func init() {
	app = fiber.New()
}

func defineEndpoints() {
	app.Get("/token", getTokenHandler)
	app.Get("/.well-known/openid-configuration", getWellKnownHandler)
	app.Head("/health_check", getHealthCheckHandler)
}

func getHealthCheckHandler(c *fiber.Ctx) error {
	return c.SendStatus(fiber.StatusOK)
}

func getTokenHandler(c *fiber.Ctx) error {
	authBody := new(entity.Authentication)

	if err := c.BodyParser(authBody); err != nil {
		return err
	}
	log.Debugf("got request at /token endpoint with body: %#v", authBody)

	isValid, err := auth.Check(c.Context(), authBody)
	if err != nil {
		return err
	}

	if isValid {
		newToken, err := token.New(context.Background())
		if err != nil {
			log.Errorf("error occured while retrieving token from authentication server: %v", err)
			return fiber.NewError(fiber.StatusInternalServerError, "could not retrieve token from authentication server")
		}
		if _, err := auth.Delete(c.Context(), authBody); err != nil {
			log.Errorf("error occured while deleting authentication code from database: %v", err)
			return fiber.NewError(fiber.StatusInternalServerError, "could not delete authentication code from database")
		}
		return c.JSON(newToken)
	} else {
		return fiber.NewError(fiber.StatusUnauthorized, "authentication code and pin are not valid")
	}
}

func getWellKnownHandler(c *fiber.Ctx) error {
	return c.JSON(config.CurrentPreAuthBridgeConfig.WellKnown)
}

func startRest(port int, wg *sync.WaitGroup) {
	log.Info("start serving rest endpoints!")
	defer wg.Done()

	defineEndpoints()
	err := app.Listen(":" + strconv.Itoa(port))
	if err != nil {
		panic(err)
	}
}

func shutdown() {
	if err := app.Shutdown(); err != nil {
		log.Fatal("Error shutting down server:", err)
	}
	log.Info("Server stopped")
}
