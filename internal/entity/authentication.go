package entity

type Authentication struct {
	Code string `json:"code"`
	Pin  string `json:"pin"`
}
