package config

import (
	"errors"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"strings"
)

type preAuthBridgeConfiguration struct {
	LogLevel string `mapstructure:"logLevel"`
	Port     int    `mapstructure:"servingPort"`
	Database struct {
		Username string `mapstructure:"username"`
		Password string `mapstructure:"password"`
		Host     string `mapstructure:"host"`
		Port     int    `mapstructure:"port"`
		Db       string `mapstructure:"db"`
	} `mapstructure:"database"`
	DefaultTtlInMin int    `mapstructure:"defaultTtlInMin"`
	OfferTopic      string `mapstructure:"offerTopic"`
	TwoFactorTopic  string `mapstructure:"twoFactorTopic"`
	OAuth           struct {
		ServerUrl    string `mapstructure:"serverUrl"`
		ClientId     string `mapstructure:"clientId"`
		ClientSecret string `mapstructure:"clientSecret"`
	} `mapstructure:"oAuth"`
	WellKnown struct {
		Issuer              string   `mapstructure:"issuer"`
		TokenEndpoint       string   `mapstructure:"token_endpoint"`
		GrantTypesSupported []string `mapstructure:"grant_types_supported"`
	} `mapstructure:"wellKnown"`
}

var CurrentPreAuthBridgeConfig preAuthBridgeConfiguration

func LoadConfig() error {
	setDefaults()
	readConfig()

	var configuredGrantTypesSupported = viper.Get("wellKnown.grant_types_supported").([]interface{})

	if len(configuredGrantTypesSupported) > 1 {
		return errors.New("unsupported config option: grant_types_supported should only have 1 value")
	}

	if configuredGrantTypesSupported[0] != "urn:ietf:params:oauth:grant-type:pre-authorized_code" {
		return errors.New("unsupported config option: grant_types_supported only supports \"urn:ietf:params:oauth:grant-type:pre-authorized_code\"")
	}

	if err := viper.Unmarshal(&CurrentPreAuthBridgeConfig); err != nil {
		return err
	}
	setLogLevel()
	return nil
}

func readConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	viper.SetEnvPrefix("PREAUTHBRIDGE")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	if err := viper.ReadInConfig(); err != nil {
		var configFileNotFoundError viper.ConfigFileNotFoundError
		if errors.As(err, &configFileNotFoundError) {
			log.Printf("Configuration not found but environment variables will be taken into account.")
		}
	}
	viper.AutomaticEnv()
}

func setDefaults() {
	viper.SetDefault("logLevel", "info")
	viper.SetDefault("servingPort", 8080)
	viper.SetDefault("databaseUrl", "redis://root:pass@redis_db:6379/0")
	viper.SetDefault("defaultTtlInMin", 30)
	viper.SetDefault("offerTopic", "offer")
	viper.SetDefault("twoFactorTopic", "pin")

	// oAuth default values
	viper.SetDefault("oAuth.serverUrl", "http://hydra:4444/oauth2/token")
	viper.SetDefault("oAuth.clientId", "bridge")
	viper.SetDefault("oAuth.clientSecret", "secret")

	// .wellknown endpoint default
	viper.SetDefault("wellKnown.issuer", "http://localhost:8080")
	viper.SetDefault("wellKnown.token_endpoint", "http://localhost:8080/token")
	viper.SetDefault("wellKnown.grant_types_supported", []string{"urn:ietf:params:oauth:grant-type:pre-authorized_code"})
}

func setLogLevel() {
	var logLevel log.Level
	switch strings.ToLower(CurrentPreAuthBridgeConfig.LogLevel) {
	case "trace":
		logLevel = log.TraceLevel
	case "debug":
		logLevel = log.DebugLevel
	case "info":
		logLevel = log.InfoLevel
	case "error":
		logLevel = log.ErrorLevel
	case "fatal":
		logLevel = log.FatalLevel
	case "panic":
		logLevel = log.PanicLevel
	default:
		logLevel = log.WarnLevel
	}
	log.SetLevel(logLevel)
	log.Infof("loglevel set to %s", logLevel.String())
}
